# Projet PostgreSQL ETC Patroni

Auto deployment for a postgresql cluster with patroni haproxy and etcd

## Infrastructure

Infrastructure is made of 8 servers (will try to migrate to micro-services and deploy on k8s)
- 3 postgres servers 1 master 2 slaves
- 2 loadbalancers with haproxy and keepalived
- 3 etcd servers to monitor database state and data

### Usage

## Ansible

### Variables

Variables are declared in group_vars directory

exemple:

```
---
################ PostgresSQL variables #####################

postgresql_dbuser: db_user
postgresql_db: my_db
postgresql_dbuser_password: "MyPassword" #must be encrypted
pgsql_vip_ip_address: 192.168.1.100
postgres_vip_port: 5000

```

### Usage

Pour lancer la configuration automatique des serveur exécutez

`ansible-playbookk -i inventory.yml site.yml`

## TODO

Faire en sorte à avoir des modules réutilisables.
